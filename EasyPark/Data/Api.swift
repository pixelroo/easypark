import Foundation

class Api {
    
    typealias CityDataHandler = (Bool, [CityViewModel]?, Error?) -> Void
    private typealias RawDataHandler = (Bool, AnyObject?, Error?) -> Void
    
    public static let shared = Api()
    
    private let errorUnknown = NSError(domain: "", code: 500, userInfo: [ NSLocalizedDescriptionKey: Strings.Errors.unknownError])
    private let errorInvalidUrl = NSError(domain: "", code: 404, userInfo: [ NSLocalizedDescriptionKey: Strings.Errors.invalidUrl])
    private let baseUrl = "https://pgroute-staging.easyparksystem.net"
    
    enum ApiEndPoint: String {
        case cities = "/cities"
    }
    
    enum ApiHttpMethod: String {
        case get = "GET"
        case post = "POST"
    }
    
    enum ApiResponseStatus: String {
        case success = "success"
    }
    
    class func createError(from value: String) -> NSError {
        return NSError(domain: "", code: 500, userInfo: [ NSLocalizedDescriptionKey: value])
    }
    
    public func getCities(handler: @escaping CityDataHandler) {
        self.dataTask(endpoint: .cities, method: .get) { success, data, error in
            if success, let data = data as? ApiResponseData, let cities = data.cities {
                let viewModels = cities.map { CityViewModel(model: $0) }
                OnMainQueue { handler(true, viewModels, nil) }
            } else {
                OnMainQueue { handler(false, nil, error ?? self.errorUnknown) }
            }
        }
    }
    
    private func dataTask(endpoint: ApiEndPoint, method: ApiHttpMethod, handler: @escaping RawDataHandler) {
        guard let urlRequest = self.createUrlRequest(endpoint: endpoint, method: method) else {
            handler(false, nil, self.errorInvalidUrl)
            return
        }

        let session = URLSession(configuration: URLSessionConfiguration.default)
        session.dataTask(with: urlRequest) { (data, response, error) -> Void in
            
            var httpResponseCode = 0
            if let response = response as? HTTPURLResponse {
                httpResponseCode = response.statusCode
            }
            
            if let data = data {
                guard let responseData = try? JSONDecoder().decode(ApiResponseData.self, from: data) else {
                    handler(false, nil, error)
                    return
                }
                
                if responseData.status != ApiResponseStatus.success.rawValue {
                    if let message = responseData.message {
                        handler(false, nil, Api.createError(from: message))
                    } else {
                        handler(false, nil, error ?? self.errorUnknown)
                    }
                    return
                }
                
                if 200...299 ~= httpResponseCode {
                    handler(true, responseData, nil)
                } else {
                    handler(false, nil, error ?? self.errorUnknown)
                }
            } else {
                handler(false, nil, error ?? self.errorUnknown)
            }
        }.resume()
    }
    
    private func createUrlRequest(endpoint: ApiEndPoint, method: ApiHttpMethod) -> URLRequest? {
        guard let url = URL(string: self.baseUrl + endpoint.rawValue) else {
            return nil
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 20
        return urlRequest
    }
}
