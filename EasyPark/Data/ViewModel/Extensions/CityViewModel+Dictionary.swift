import Foundation
import MapKit


typealias CityData = [CityViewModel]

extension CityData {
    
    enum SortType {
        case name
        case distance
    }
    
    mutating func recalculateDistances(userLocation: CLLocation? = nil) {
        self.forEach{ city in
            city.calculateDistanceFromUser(userLocation: userLocation)
        }
    }
    
    func getCityBy(name: String) -> CityViewModel? {
        return self.first(where: {$0.name == name})
    }
    
    func filterCitiesBy(name term: String, sortType: SortType? = nil) -> CityData {
        let filteredCities = self.filter {
            if let cityName = $0.name?.lowercased() {
                return cityName.contains(term.lowercased())
            }
            return false
        }
        
        guard let sortType = sortType else { return filteredCities }
        
        switch sortType {
        case .distance: return getCitiesSortedByDistance(cityData: filteredCities)
        case .name: return getCitiesSortedOnName(cityData: filteredCities)
        }
        
    }
    
    func getCitiesSortedOnName(cityData: CityData? = nil) -> CityData {
        if let cityData = cityData {
            return cityData.sorted(by: self.nameSorter)
        }
        return self.sorted(by: self.nameSorter)
    }
    
    func getCitiesSortedByDistance(cityData: CityData? = nil) -> CityData {
        if let cityData = cityData {
            return cityData.sorted(by: self.distanceSorter)
        }
        return self.sorted(by: self.distanceSorter)
    }
    
    private func nameSorter(lhs: CityViewModel, rhs:CityViewModel) -> Bool {
        if let lhs = lhs.name, let rhs = rhs.name { return lhs.lowercased() < rhs.lowercased() }
        else { return false }
    }
    
    private func distanceSorter(lhs: CityViewModel, rhs:CityViewModel) -> Bool {
        if let lhs:Int = lhs.getDistance(), let rhs:Int = rhs.getDistance() { return lhs < rhs }
        else { return false }
    }
    
}
