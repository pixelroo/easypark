import Foundation
import CoreGraphics
import CoreLocation
import MapKit

class CityViewModel: CustomStringConvertible {
    
    private static var numberFormatter: NumberFormatter  {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter
    }
    
    private let model: CityModel
    private var distanceFromUser: Int = -1
    private var hasError: Bool = false
    private var errorMessage: String?
    
    private var pointData: [CLLocation] = []
    private(set) var location: CLLocation?
    private(set) var annotation: MKAnnotation?
    private(set) var mapOverlay: MKPolygon?
    
    init(model: CityModel) {
        self.model = model
        self.calculateDistanceFromUser()
        self.location = self.createPosition()
        self.annotation = self.createAnnotation()
        self.pointData = self.createPoints(model.points)
        self.mapOverlay = self.createOverlay()
    }
    
    var name: String? {
        get { return self.model.name }
    }
    
    var lat: Double? {
        get { return self.model.lat }
    }
    
    var lon: Double? {
        get { return self.model.lon }
    }
    
    var r: Int? {
        get { return self.model.r }
    }
    
    var points: [CLLocation]? {
        get {
            if self.hasError { return nil }
            return self.pointData
        }
    }
    
    class func createOverlayRenderer(overlay: MKOverlay) -> MKPolygonRenderer? {
        if overlay is MKPolygon {
            let overlayRenderer = MKPolygonRenderer(overlay: overlay)
            overlayRenderer.strokeColor = UIColor.Theme.Purple
            overlayRenderer.fillColor = UIColor.Theme.Purple.withAlphaComponent(0.3)
            overlayRenderer.lineWidth = 4
            return overlayRenderer
        }
        return nil
    }
    
    private func createOverlay() -> MKPolygon? {
        guard let points = self.points, !points.isEmpty else { return nil }
        
        var cityPoints = points.map {$0.coordinate}
        cityPoints.append(points.first!.coordinate)
        let polygon = MKPolygon(coordinates: &cityPoints, count: cityPoints.count)
        return polygon
    }
    
    private func createAnnotation() -> MKPointAnnotation? {
        guard let location = location else { return nil }
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        annotation.title = self.name
        annotation.subtitle = self.getDistance()
        return annotation
    }
    
    private func createPosition() -> CLLocation? {
        guard let lat = self.lat,
              let lon = self.lon
        else { return nil }
        
        return CLLocation(latitude: lat, longitude: lon)
    }
    
    func calculateDistanceFromUser(userLocation: CLLocation? = nil) {
        guard let userLocation = userLocation,
              let location = self.location
        else { return }
        
        self.distanceFromUser = LocationManager.shared.calulateDistance(from: userLocation, to: location)
        self.annotation = self.createAnnotation()
    }
    
    func getDistance() -> Int? {
        if self.distanceFromUser == -1 { return nil }
        return distanceFromUser
    }
    
    func getDistance() -> String {
        if self.distanceFromUser == -1 { return Strings.Map.NoGPS }
        
        if let formattedString = CityViewModel.numberFormatter.string(from: NSNumber(value: self.distanceFromUser)) {
            return formattedString + Strings.Map.kilometers
        }
        return String(self.distanceFromUser) + Strings.Map.kilometers
    }
    
    private func createPoints(_ points: String?) -> [CLLocation] {
        guard let points = points else {
            self.setToErrorState(with: Strings.Errors.pointDataFail)
            return []
        }
        
        var output: [CLLocation] = []
        
        let pointStrings = points.components(separatedBy: ",")
        pointStrings.forEach { pointString in
            let pointData = pointString.components(separatedBy: " ")
            if pointData.indices.contains(1) {
                if let xValue = Double(pointData[1]),
                   let yValue = Double(pointData[0]) {
                    output.append(CLLocation(latitude: xValue, longitude: yValue))
                }
            }
        }
        
        if output.count != pointStrings.count {
            self.setToErrorState(with: Strings.Errors.pointDataIncomplete)
            return []
        }
        
        return output
    }
    
    private func setToErrorState(with message: String) {
        self.hasError = true
        self.errorMessage = message
    }
    
    public var description: String  {
        var description = "\n-- City VM--\n"
        description += "name: \(self.stringOf(self.name))"
        description += "lat: \(self.stringOf(self.lat))"
        description += "lon: \(self.stringOf(self.lon))"
        description += "r: \(self.stringOf(self.r))"
        description += "hasError: \(self.stringOf(self.hasError))"
        description += "errorMessage: \(self.stringOf(self.errorMessage))"
        
        description += "points:\n"
        self.points?.forEach {
            description += "__\(self.stringOf($0))"
        }
        return description
    }
    
    private func stringOf(_ value: Any?) -> String {
        guard let value = value else {return "nil\n" }
        return String(describing: value) + "\n"
    }
}
