public class ApiResponseData: Decodable, CustomStringConvertible {
    
    public var status: String?
    public var message: String?
    public var cities: [CityModel]?
    
    enum CodingKeys : String, CodingKey {
        case status
        case message
        case cities
    }
    
    public var description: String {
        return "- API RESPONSE -"
    }
    
}
