import Foundation
import CoreGraphics

public class CityModel: Decodable {
    
    public var name: String?
    public var lat: Double?
    public var lon: Double?
    public var r: Int?
    public var points: String?
    
    enum CodingKeys : String, CodingKey {
        case name
        case lat
        case lon
        case r
        case points
    }
}
