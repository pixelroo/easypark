import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    public static let shared = LocationManager()
    
    let manager: CLLocationManager = CLLocationManager()
    var onLocationStatus: (PermissionStatus) -> () = { _ in }
    var onLocationData: (CLLocation) -> () = { _ in }
    var onLocationError: (Error) -> () = {_ in }
    
    override init() {
        super.init()
        self.configureManager()
    }
    
    private func configureManager() {
        self.manager.delegate = self
    }
    
    internal func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var appStatus: PermissionStatus = .unknown
        
        switch status {
        case .authorizedWhenInUse:
            appStatus = .authorized
            self.requestLocation()
        case .denied: appStatus = .denied
        default: appStatus = .unknown
        }
        
        OnMainQueue {
            self.onLocationStatus(appStatus)
        }
    }
    
    func requestLocation() {
        self.manager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        OnMainQueue {
            self.onLocationError(error)
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.onLocationData(location)
        }
    }
    
    func calulateDistance(from startLocation: CLLocation, to endLocation: CLLocation) -> Int {
        return Int((startLocation.distance(from: endLocation) / 1000).rounded())
    }
}
