import Foundation
import CoreLocation

enum PermissionType {
    case location
}

enum PermissionStatus {
    case authorized
    case denied
    case unknown
}

class Permissions {
    
    public static let shared = Permissions()
    
    func hasPermissionFor(type: PermissionType, completion: @escaping (PermissionStatus) -> ()) {
        var result: PermissionStatus = .denied
        
        switch type {
        case .location:
            switch LocationManager.shared.manager.authorizationStatus {
            case .authorizedWhenInUse: result = .authorized
            case .denied: result = .denied
            default: result = .unknown
            }
        }
        
        OnMainQueue {
            completion(result)
        }
    }
    
    func getPermissionFor(type: PermissionType, completion: @escaping (PermissionStatus) -> ()) {
        switch type {
        case .location:
            LocationManager.shared.onLocationStatus = completion
            LocationManager.shared.manager.requestWhenInUseAuthorization()
        }
    }
}
