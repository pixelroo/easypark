import Foundation
import UIKit

func openAppSettings(completion: @escaping (Bool) -> ()) {
    let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
    UIApplication.shared.open(
        settingsAppURL, options: [:],
        completionHandler: completion
    )
}
