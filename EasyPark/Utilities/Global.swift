import Foundation

func OnMainQueue(completion: @escaping ()->() ) {
    DispatchQueue.main.async {
        completion()
    }
}
