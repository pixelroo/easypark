import Foundation
import UIKit

extension UIViewController {
    
    internal func addHidingKeyboardTouch(preventTouches: Bool = true) {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTap))
        gesture.numberOfTapsRequired = 1
        gesture.cancelsTouchesInView = preventTouches
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(gesture)
    }

    @objc internal func dismissOnTap() {
        self.view.endEditing(true)
    }
    
    internal func addKeyboardObservers(_ value: Bool) {
        if value {
            NotificationCenter.default.addObserver(
                self, selector: #selector(self.keyboardWillShow),
                name: UIResponder.keyboardWillShowNotification,
                object: nil
            )
            
            NotificationCenter.default.addObserver(
                self, selector: #selector(self.keyboardWillHide),
                name: UIResponder.keyboardWillHideNotification,
                object: nil
            )
        } else {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        var gap = CGFloat(0)
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            gap = keyboardSize.height
        }
        
        var curve = 7
        var duration = 0.25
        
        if let animationDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber) {
            duration = animationDuration.doubleValue
        }
        
        if let animationCurve = (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber) {
            curve = animationCurve.intValue
        }
        
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: UInt(curve << 16)),
            animations: {
                self.onKeyboardAppear(gap: gap)
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        var curve = 7
        var duration = 0.25
        
        if let animationDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber) {
            duration = animationDuration.doubleValue
        }
        
        if let animationCurve = (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber) {
            curve = animationCurve.intValue
        }
        
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: UInt(curve << 16)),
            animations: {
                self.onKeyboardDisappear()
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    @objc func onKeyboardAppear(gap: CGFloat) { }
    
    @objc func onKeyboardDisappear() { }
}
