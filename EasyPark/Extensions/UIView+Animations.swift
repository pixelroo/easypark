import Foundation
import UIKit

extension UIView {
    
    func fadeOutWithScale() {
        UIView.animate(
            withDuration: Animations.Basic.duration,
            animations: {
                self.transform = Animations.Basic.transformLarge
                self.alpha = Animations.Basic.alphaNone
            }
        )
    }
    
    func fadeInWithScale() {
        self.transform = Animations.Basic.transformLarge
        self.alpha = Animations.Basic.alphaNone
        UIView.animate(
            withDuration: Animations.Basic.duration,
            animations: {
                self.transform = Animations.Basic.transformNomral
                self.alpha = Animations.Basic.alphaFull
            }
        )
    }
    
    func animateLayout(animations: (()->())? = nil, completion:(()->())? = nil) {
        UIView.animate(
            withDuration: Animations.Basic.duration,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                animations?()
                self.setNeedsLayout()
                self.layoutIfNeeded()
            },
            completion: { _ in completion?()}
        )
    }
}
