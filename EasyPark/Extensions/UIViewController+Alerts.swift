import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(error: Error?) {
        OnMainQueue {
            let alert = UIAlertController(
                title: Strings.Errors.errorTitle,
                message: error?.localizedDescription ?? Strings.Errors.unknownError,
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: Strings.Errors.errorButtonLabel, style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

