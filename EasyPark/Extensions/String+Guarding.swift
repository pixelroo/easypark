import Foundation


extension String {
    
    func isNilOrEmpty() -> Bool {
        let string = self
        return string == nil || self.isEmpty
    }
    
}
