import Foundation
import UIKit

extension UIColor {
    
    static private let bundle = Bundle(identifier: "pixel.roo.EasyPark")
    
    enum Theme {
        static let GreyDark = UIColor(named: "GreyDark", in: bundle, compatibleWith: nil)!
        static let Pink = UIColor(named: "Pink", in: bundle, compatibleWith: nil)!
        static let Purple = UIColor(named: "Purple", in: bundle, compatibleWith: nil)!
    }
    
}
