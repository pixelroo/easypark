import Foundation
import UIKit

extension UIViewController {
    
    internal func setForegroundObservers(_ add: Bool) {
        if add {
            NotificationCenter.default.addObserver(
                self, selector: #selector(self.onForeground),
                name: UIApplication.willEnterForegroundNotification,
                object: nil
            )
        } else {
            NotificationCenter.default.removeObserver(
                self, name: UIApplication.willEnterForegroundNotification,
                object: nil
            )
        }
    }
    
    @objc internal func onForeground() {}
}
