import Foundation
import UIKit

extension UIView {
    
    func maskTopCorners(by value: CGFloat) {
        self.layer.cornerRadius = value
        self.layer.masksToBounds = true
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func maskAllCorners(by value: CGFloat) {
        self.layer.cornerRadius = value
//        self.clipsToBounds = true
    }
    
}
