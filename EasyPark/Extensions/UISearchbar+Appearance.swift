import Foundation
import UIKit

extension UISearchBar {
    
    func style() {
        self.searchTextField.backgroundColor = .white
        self.searchTextField.tintColor = UIColor.Theme.Pink
        self.searchTextField.textColor = UIColor.Theme.GreyDark
        self.searchTextField.clearButtonMode = .never
        
    }
    
}
