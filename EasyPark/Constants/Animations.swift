import Foundation
import CoreGraphics

struct Animations{
    struct Basic {
        static let alphaFull: CGFloat = 1.0
        static let alphaHalf: CGFloat = 0.5
        static let alphaNone: CGFloat = 0.0
        static let duration: Double = 0.25
        static let transformLarge = CGAffineTransform(scaleX: 1.3, y: 1.3)
        static let transformNomral = CGAffineTransform(scaleX: 1.0, y: 1.0)
    }
}
