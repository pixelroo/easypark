import Foundation

struct Strings {
    
    struct Map {
        static let NoGPS: String = "GPS not enabled"
        static let kilometers: String = " km"
    }
    
    struct Xibs {
        struct SearchView {
            static let cityTableCell: String = "CityTableCell"
        }
    }
    
    struct Segues {
        static let mainToPermission: String = "mainPermissionSegue"
        static let embedSearchSegue: String = "embedSearchSegue"
    }
    
    struct SearchView {
        static let infoWhenCollapsed: String = "Tap here to search"
        static let infoWhenExpanded: String = "Tap here to close"
    }
    
    struct LocationOptionsView {
        static let showUserInRegion: String = "Show user in region"
        static let hideUserInRegion: String = "Hide user in region"
    }
    
    struct Errors {
        static let errorTitle: String = "Oh no!"
        static let errorButtonLabel: String = "Ok"
        static let unknownError: String = "Unexpected error."
        static let invalidUrl: String = "The URL is invalid."
        static let pointDataFail: String = "There is a problem with the gps data."
        static let pointDataIncomplete: String = "There is a problem with some of the gps data."
    }
    
}
