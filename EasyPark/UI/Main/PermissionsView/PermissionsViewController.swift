import Foundation
import UIKit

@IBDesignable
class PermissionsViewController: UIViewController {
    
    @IBOutlet var permissionButton: EasyParkButton!
    @IBOutlet var completeButton: EasyParkButton!
    
    var onPermissionStatus: (() -> ()) = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setForegroundObservers(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.setForegroundObservers(false)
    }
    
    override func onForeground() {
        Permissions.shared.getPermissionFor(type: .location, completion: { status in
            if status == .authorized {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    private func configureButtons() {
        self.permissionButton.setAction(action: self.onPermissionPress)
        self.completeButton.setAction(action: self.onCompletePress)
    }
    
    private func onCompletePress() {
        self.dismiss(
            animated: true,
            completion: {
                self.onPermissionStatus()
            }
        )
    }
    
    private func onPermissionPress() {
        Permissions.shared.hasPermissionFor(type: .location, completion: { status in
            switch status {
            case .denied: self.navigateToAppSettings()
            case .unknown: self.requestPermission()
            default: return
            }
        })
    }
    
    private func navigateToAppSettings() {
        openAppSettings() { _ in }
    }
    
    private func requestPermission() {
        Permissions.shared.getPermissionFor(
            type: .location,
            completion: { status in
                if status == .authorized {
                    self.dismiss(
                        animated: true,
                        completion: {
                            self.onPermissionStatus()
                        }
                    )
                }
            }
        )
    }
}
