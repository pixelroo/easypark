import Foundation
import MapKit

class MapDelegate: NSObject, MKMapViewDelegate {
    
    typealias onLoadingHandler = (Bool) -> ()
    typealias onErrorHandler = (Error?) ->()
    typealias onDataHandler = ([CityViewModel]) -> ()
    
    let mapView: MKMapView
    
    private var userLocation: CLLocation?
    private var annotations: [MKAnnotation] = []
    private var cityData: [CityViewModel] = []
    private var currentCityViewModel: CityViewModel?
    private var existingOverlay: MKPolygon?
    private var currentAnnotationTitle: String?
    private var showUser: Bool = false
    
    private var lineOverlay: MKPolyline?
    
    init(mapView: MKMapView) {
        self.mapView = mapView
        super.init()
        self.setup()
    }
    
    func setup() {
        self.mapView.delegate = self
        self.mapView.mapType = .standard
        self.mapView.showsScale = true
        self.mapView.showsCompass = true
        self.mapView.showsUserLocation = true
    }
    
    func updateData(cityData: CityData, userLocation: CLLocation?) {
        self.cityData = cityData
        self.resetAnnotations()
        self.userLocation = userLocation
        self.addLocationPoints()
    }
    
    private func resetAnnotations() {
        self.mapView.removeAnnotations(self.annotations)
        self.annotations.removeAll()
    }
    
    func focusToUser() {
        if let userLocation = self.userLocation {
            self.focusToCoordinate(coordinate: userLocation.coordinate, force: true)
        }
    }
    
    func toggleLines(showUser: Bool) {
        self.showUser = showUser
        
        if !self.showUser {
            self.clearDistanceLine()
        } else {
            self.drawDistanceLine()
        }
        
        if let cityCoordinate = self.currentCityViewModel?.location?.coordinate {
            self.focusToCoordinate(coordinate: cityCoordinate)
        }
    }
    
    func clearDistanceLine() {
        if let existingLine = self.lineOverlay {
            self.mapView.removeOverlay(existingLine)
            self.lineOverlay = nil
        }
    }
    
    func drawDistanceLine() {
        self.clearDistanceLine()
        
        guard let userCoordinate = self.userLocation?.coordinate,
              let cityCoordinate = self.currentCityViewModel?.location?.coordinate
        else {
            return
        }
        
        self.lineOverlay = MKPolyline(
            coordinates: [
                userCoordinate,
                cityCoordinate
            ],
            count: 2
        )
        self.mapView.addOverlay(self.lineOverlay!)
    }
    
    func focusToCity(cityViewModel: CityViewModel) {
        if let annotation = cityViewModel.annotation {
            self.focusToCoordinate(coordinate: annotation.coordinate)
            self.mapView.selectAnnotation(annotation, animated: true)
        }
        self.drawOutline(cityViewModel: cityViewModel)
    }
    
    func addLocationPoints() {
        if !annotations.isEmpty {
            return
        }
        
        self.cityData.forEach { city in
            if let annotation = city.annotation {
                annotations.append(annotation)
            }
        }
        
        self.mapView.addAnnotations(self.annotations)
        
        if let userLocation = self.userLocation {
            if let annotation = self.annotations.first(where: {$0.title == currentAnnotationTitle}) {
                self.mapView.selectAnnotation(annotation, animated: true)
            } else {
                self.focusToCoordinate(coordinate: userLocation.coordinate, animated: false)
            }
        } else if let firstCity = self.cityData.first, let annotation = firstCity.annotation {
            self.mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    private func drawOutline(cityViewModel: CityViewModel) {
        if let existingOverlay = self.existingOverlay {
            self.mapView.removeOverlay(existingOverlay)
        }
        
        if let overlay = cityViewModel.mapOverlay {
            self.existingOverlay = overlay
            self.mapView.addOverlay(overlay)
        } else {
            self.existingOverlay = nil
        }
    }
    
    func createLineRenderer(overlay: MKOverlay) -> MKPolylineRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.lineWidth = 2
        renderer.strokeColor = UIColor.Theme.Purple
        renderer.lineCap = .round
        return renderer
    }
    
    internal func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation,
              let title = annotation.title,
              let safeTitle = title,
              let cityViewModel = self.cityData.getCityBy(name: safeTitle),
              let coordinate = cityViewModel.location?.coordinate
        else { return }
        
        self.currentCityViewModel = cityViewModel
        self.currentAnnotationTitle = title
        self.drawOutline(cityViewModel: cityViewModel)
        self.focusToCoordinate(coordinate: coordinate)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            return createLineRenderer(overlay: overlay)
        }
        
        if overlay is MKPolygon, let renderer = CityViewModel.createOverlayRenderer(overlay: overlay) {
            return renderer
        }
        return MKPolylineRenderer(overlay: overlay)
    }
    
    private func focusToCoordinate(coordinate: CLLocationCoordinate2D, animated: Bool = true, force: Bool = false) {
        if !force && self.showUser {
            guard  let userCoordinate = self.userLocation?.coordinate,
                   let cityAnnotation = self.currentCityViewModel?.annotation
            else {
                return
            }
            let userAnnotation = MKPointAnnotation()
            userAnnotation.coordinate = userCoordinate
            
            self.mapView.showAnnotations(
                [userAnnotation, cityAnnotation],
                animated: true
            )
            
            self.mapView.removeAnnotation(userAnnotation)
            
            self.drawDistanceLine()
            
        } else {
            self.mapView.setRegion(
                MKCoordinateRegion(
                    center: coordinate,
                    span: MKCoordinateSpan(
                        latitudeDelta: CLLocationDegrees(0.2),
                        longitudeDelta: CLLocationDegrees(0.2)
                    )
                ),
                animated: animated
            )
        }
    }
}
