import Foundation
import UIKit

@IBDesignable
class LocationOptions: UIView, NibLoadable {
    
    @IBOutlet var recenterButton: EasyParkButton!
    @IBOutlet var showlinesButton: EasyParkButton!
    
    private var isShowingUser = false
    private var lineAction: (Bool) -> () = {_ in}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit() {
        self.setupFromNib()
        self.toggleDisplayState()
        self.setButtonLabels()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addShadow()
    }
    
    func setRecenterAction(action: @escaping ()->()) {
        self.recenterButton.setAction (action: action)
    }
    
    func setLineAction(action: @escaping(Bool) ->()) {
        self.lineAction = action
        self.showlinesButton.setAction(action: self.toggleUser)
    }
    
    func setButtonLabels() {
        self.showlinesButton.buttonLabelText = self.isShowingUser
            ? Strings.LocationOptionsView.hideUserInRegion
            : Strings.LocationOptionsView.showUserInRegion
    }
    
    private func toggleUser() {
        self.isShowingUser = !self.isShowingUser
        self.setButtonLabels()
        self.lineAction(self.isShowingUser)
    }
    
    func toggleDisplayState() {
        Permissions.shared.hasPermissionFor(
            type: .location,
            completion: { status in
                if status == .authorized {
                    self.fadeInWithScale()
                } else {
                    self.fadeOutWithScale()
                }
            }
        )
    }
}
