import Foundation
import UIKit

@IBDesignable
class LocationWarningView: UIView, NibLoadable {
    
    @IBOutlet var bgView: UIView!
    
    private var buttonAction: ()->() = {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit() {
        self.setupFromNib()
        self.toggleDisplayState()
    }
    
    func toggleDisplayState() {
        Permissions.shared.hasPermissionFor(
            type: .location,
            completion: { status in
                if status == .authorized {
                    self.fadeOutWithScale()
                }
            }
        )
    }
    
    override func draw(_ rect: CGRect) {
        self.bgView.maskAllCorners(by: self.frame.height * 0.5)
        self.addShadow()
        super.draw(rect)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setAction(action: @escaping ()->()) {
        self.buttonAction = action
    }
    
    @IBAction func onButtonPress(_ sender: Any) {
        self.buttonAction()
    }
}

