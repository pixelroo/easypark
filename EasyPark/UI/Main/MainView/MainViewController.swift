import Foundation
import UIKit
import MapKit

@IBDesignable
class MainViewController: UIViewController {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var locationWarningView: LocationWarningView!
    @IBOutlet var locationOptionsView: LocationOptions!
    @IBOutlet var searchConstraint: NSLayoutConstraint!
    @IBOutlet var searchContainer: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    private var searchViewController: SearchViewController?
    private var dataManager: CityDataManager?
    private var mapDelegate: MapDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureContainer()
        self.configureMap()
        self.configureDataManager()
        self.configureLocationManager()
        self.configureButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setForegroundObservers(true)
        self.searchViewController?.expandableConstraint = self.searchConstraint
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.setForegroundObservers(false)
    }
    
    private func configureContainer() {
        self.searchContainer.maskTopCorners(by: 14)
    }
    
    private func configureButtons() {
        self.locationWarningView.setAction(action: self.segueToPermissions)
        self.locationOptionsView.setRecenterAction(action: self.mapDelegate?.focusToUser ?? {})
        self.locationOptionsView.setLineAction(action: self.mapDelegate?.toggleLines ?? {_ in})
    }
    
    private func configureMap() {
        self.mapDelegate = MapDelegate(mapView: self.mapView)
    }
    
    private func configureDataManager() {
        self.dataManager = CityDataManager(
            onLoadingState: self.onLoadingState,
            onDataError: self.onDataError,
            onDataSuccess: self.onDataSuccess
        )
    }
    
    private func configureLocationManager() {
        LocationManager.shared.onLocationError = self.onLocationError
    }
    
    private func onLoadingState(_ value: Bool) {
        if value {
            self.view.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
        } else {
            self.view.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func onLocationError(error: Error) {
        self.showAlert(error: error)
    }
    
    private func onDataError(error: Error?) {
        self.showAlert(error: error)
    }
    
    private func onDataSuccess(cityData: CityData, userLocation: CLLocation?) {
        self.mapDelegate?.updateData(cityData: cityData, userLocation: userLocation)
        self.searchViewController?.updateData(cityData: cityData)
    }
    
    private func onCitySelectHandler(cityViewModel: CityViewModel) {
        self.mapDelegate?.focusToCity(cityViewModel: cityViewModel)
    }
    
    private func segueToPermissions() {
        self.performSegue(withIdentifier: Strings.Segues.mainToPermission, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Strings.Segues.embedSearchSegue:
            if let targetViewController = segue.destination as? SearchViewController {
                self.searchViewController = targetViewController
                self.searchViewController?.setSelectionHandler(onCitySelectHandler: self.onCitySelectHandler)
            }
        case Strings.Segues.mainToPermission:
            if let targetViewController = segue.destination as? PermissionsViewController {
                targetViewController.onPermissionStatus = self.onPermissionStatus
            }
        default: return
        }
    }
    
    override func onForeground() {
        self.onPermissionStatus()
    }
    
    private func onPermissionStatus() {
        self.dataManager?.genericHandler = {
            self.locationWarningView.toggleDisplayState()
            self.locationOptionsView.toggleDisplayState()
        
        }
        LocationManager.shared.requestLocation()
    }
}



