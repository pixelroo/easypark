import Foundation
import MapKit

class CityDataManager: NSObject {
    
    typealias OnLoadingState = (Bool) -> ()
    typealias OnDataError = (Error?) -> ()
    typealias OnDataSuccess = (CityData, CLLocation?) -> ()
    typealias GenericHandler = () -> ()

    public static let shared = CityDataManager()
    private var cityData: CityData = []
    
    private var onLoadingState: OnLoadingState?
    private var onDataError: OnDataError?
    private var onDataSuccess: OnDataSuccess?
    
    var genericHandler: GenericHandler?
    
    init(onLoadingState: OnLoadingState? = nil, onDataError: OnDataError? = nil, onDataSuccess: OnDataSuccess? = nil) {
        self.onLoadingState = onLoadingState
        self.onDataError = onDataError
        self.onDataSuccess = onDataSuccess
        super.init()
        self.loadData()
        LocationManager.shared.onLocationData = self.onLocationData
    }
    
    func loadData() {
        self.onLoadingState?(true)
        Api.shared.getCities { success, cities, error in
            self.onLoadingState?(false)
            if success, let cityData = cities {
                self.cityData = cityData
                Permissions.shared.hasPermissionFor(
                    type: .location,
                    completion: {status in
                        if status == .authorized {
                            LocationManager.shared.requestLocation()
                        } else {
                            self.onDataSuccess?(self.cityData, nil)
                        }
                    }
                )
            } else {
                self.onDataError?(error)
            }
        }
    }
    
    private func onLocationData(userLocation: CLLocation? = nil) {
        self.recalculateDistances(userLocation: userLocation)
        self.fireGenericaHandler()
    }
    
    private func fireGenericaHandler() {
        self.genericHandler?()
        self.genericHandler = nil
    }
    
    func recalculateDistances(userLocation: CLLocation? = nil) {
        self.cityData.recalculateDistances(userLocation: userLocation)
        self.onDataSuccess?(self.cityData, userLocation)
    }
    
}
