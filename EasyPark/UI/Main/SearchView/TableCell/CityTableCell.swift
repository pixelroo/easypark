import Foundation
import UIKit

class CityTableCell: UITableViewCell {
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelDistance: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.labelName.text = ""
        self.labelDistance.text = ""
    }
    
    func configure(data: CityViewModel) {
        self.labelName.text = data.name ?? ""
        self.labelDistance.text = data.getDistance()
    }
}
