import Foundation
import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate {
    
    typealias OnCitySelectHandler = (CityViewModel) -> ()
    
    @IBOutlet var cityTableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var labelInfo: UILabel!
    @IBOutlet var blockerView: UIView!
    
    private var onCitySelectHandler: OnCitySelectHandler?
    var constraintValueCollapsed = CGFloat(0)
    var constraintValueExpanded = CGFloat(0)
    var cityData: CityData = []
    var filteredData: CityData = []
    var isFiltering: Bool = false
    var isExpanded: Bool = false
    
    var panPoint = CGPoint.zero
    var originalPoint = CGFloat(0)
    var currentPoint = CGFloat(0)
    var diff = CGFloat(0)
    let thresholdForEnd = CGFloat(50.0)
    var dragging: Bool = false
    
    var expandableConstraint: NSLayoutConstraint? {
        didSet {
            self.constraintValueCollapsed = self.expandableConstraint?.constant ?? 0
            self.constraintValueExpanded = -self.view.frame.height
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSearch()
        self.registerCells()
        self.configureTable()
        self.addKeyboardObservers(true)
        self.addHidingKeyboardTouch(preventTouches: false)
    }
    
    private func configureSearch() {
        self.searchBar.style()
        self.searchBar.delegate = self
    }
    
    private func registerCells() {
        self.cityTableView.register(
            UINib(nibName: Strings.Xibs.SearchView.cityTableCell, bundle: nil),
            forCellReuseIdentifier: Strings.Xibs.SearchView.cityTableCell
        )
    }
    
    private func configureTable() {
        self.cityTableView.tableHeaderView = UIView()
        self.cityTableView.tableFooterView = UIView()
        self.cityTableView.delegate = self
        self.cityTableView.dataSource = self
    }
    
    func setSelectionHandler(onCitySelectHandler: OnCitySelectHandler? = nil) {
        self.onCitySelectHandler = onCitySelectHandler
    }
    
    @IBAction func onTogglePress(_ sender: Any) {
        self.toggleExpandState()
    }
    
    @objc func onSwipeUp(_ sender: Any) {
        if self.isExpanded { return }
        self.toggleExpandState()
    }
    
    @objc func onSwipeDown(_ sender: Any) {
        if !self.isExpanded { return }
        self.toggleExpandState()
    }

    func updateData(cityData: CityData) {
        self.cityData = cityData
        
        if let searchTerm = self.searchBar.text, !searchTerm.isEmpty {
            updateFilterData(filteredData: cityData.filterCitiesBy(name: searchTerm, sortType: .name))
        } else {
            self.cityTableView.reloadData()
        }
    }
    
    func updateFilterData(filteredData: CityData) {
        self.isFiltering = true
        self.filteredData = filteredData
        self.cityTableView.reloadData()
    }
    
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchTerm = searchBar.text, !searchTerm.isEmpty {
            self.updateFilterData(filteredData: cityData.filterCitiesBy(name: searchTerm, sortType: .name))
        } else {
            self.isFiltering = false
        }
    }
        
    internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.isFiltering = false
        self.updateData(cityData: self.cityData)
        self.view.endEditing(true)
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getDataSet().count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: Strings.Xibs.SearchView.cityTableCell,
                for: indexPath) as? CityTableCell
        else {
            return UITableViewCell()
        }
        
        cell.configure(data: self.getDataSet()[indexPath.row])
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.toggleExpandState()
        self.onCitySelectHandler?(self.getDataSet()[indexPath.row])
    }
    
    private func getDataSet() -> CityData {
        return self.isFiltering
            ? self.filteredData
            : self.cityData
    }
    
    private func toggleExpandState() {
        self.isExpanded = !self.isExpanded
        
        if !self.isExpanded {
            self.view.endEditing(true)
        }
        
        self.labelInfo.text = self.isExpanded
            ? Strings.SearchView.infoWhenExpanded
            : Strings.SearchView.infoWhenCollapsed
        
        self.expandableConstraint?.constant = self.isExpanded
            ? self.constraintValueExpanded
            : self.constraintValueCollapsed
        
        self.parent?.view.animateLayout(
            animations: {
                self.blockerView.alpha = self.isExpanded
                    ? Animations.Basic.alphaNone
                    : Animations.Basic.alphaFull
            },
            completion: {
                if !self.isExpanded {
                    self.blockerView.isUserInteractionEnabled = false
                    self.cityTableView.setContentOffset(CGPoint.zero, animated: false)
                } else {
                    self.blockerView.isUserInteractionEnabled = true
                }
            }
        )
    }
    
    override func onKeyboardAppear(gap: CGFloat) {
        self.view.frame.size.height = abs(self.constraintValueExpanded) - gap
    }
    
    override func onKeyboardDisappear() {
        self.view.frame.size.height = abs(self.constraintValueExpanded)
    }
}
