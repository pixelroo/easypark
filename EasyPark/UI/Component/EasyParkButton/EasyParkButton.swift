import Foundation
import UIKit

@IBDesignable
class EasyParkButton: UIView, NibLoadable {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var button: UIButton!
    
    private var buttonAction: ()->() = {}
    
    @IBInspectable
    var bgColor: UIColor = .white {
        didSet {
            self.layoutSubviews()
        }
    }

    @IBInspectable
    var borderColor: UIColor = .white {
        didSet {
            self.layoutSubviews()
        }
    }

    @IBInspectable
    var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layoutSubviews()
        }
    }

    @IBInspectable
    var buttonLabelColor: UIColor = UIColor.Theme.Pink {
        didSet {
            self.layoutSubviews()
        }
    }
    
    @IBInspectable
    var buttonLabelText: String = "NOT SET" {
        didSet {
            self.layoutSubviews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit() {
        self.setupFromNib()
    }
    
    func setAction(action: @escaping ()->()) {
        self.buttonAction = action
    }
    
    @IBAction func onButtonPress(_ sender: Any) {
        self.buttonAction()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.button.setTitleColor(self.buttonLabelColor, for: .normal)
        self.button.setTitle(self.buttonLabelText, for: .normal)
        self.bgView.maskAllCorners(by: self.frame.height * 0.5)
        self.bgView.backgroundColor = self.bgColor
        self.bgView.layer.borderWidth = self.borderWidth
        self.bgView.layer.borderColor = self.borderColor.cgColor
    }
}
